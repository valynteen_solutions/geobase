class Geobase::Country < ActiveRecord::Base
  has_many :regions
  has_many :localities, through: :regions
end
