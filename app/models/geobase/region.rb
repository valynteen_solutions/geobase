class Geobase::Region < ActiveRecord::Base
  belongs_to :country
  belongs_to :parent, class_name: 'Geobase::Region', foreign_key: :parent_id
  has_many :localities, foreign_key: :primary_region_id

  scope :parent_code, ->(code) {
    joins(:parent).where("parents_geobase_regions.code = '#{code}'").readonly(false)
  }

  SEPARATOR = '<sep/>'

  def self.by_name(name)
  	return nil if name.blank?
  	where('LOWER(name) = ?', name.downcase).first
  end

	def self.by_id(id)
    return all unless id.present?
    where("geobase_regions.id = ?", id)
  end

  def self.by_name_field(name)
    return all unless name.present?
    where('LOWER(geobase_regions.name) LIKE ?', "%#{name.downcase}%")
  end

  def self.locality_and_region_name(locality_name, region_name)
    joins('INNER JOIN localities ON (geobase_localities.primary_region_id = regions.id)').where("LOWER(geobase_localities.name) = ? AND LOWER(geobase_regions.name) = ?", locality_name.downcase, region_name.downcase).readonly(false).first
  end

  def nickname_array
    nicknames.to_s.split(SEPARATOR)
  end

  def random_nickname
    nickname_array[rand(nicknames_array.length)]
  end
end
