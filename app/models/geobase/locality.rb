class Geobase::Locality < ActiveRecord::Base
  belongs_to :primary_region, class_name: 'Geobase::Region', foreign_key: :primary_region_id
  has_one :country, through: :primary_region
  has_and_belongs_to_many :zip_codes

  SEPARATOR = '<sep/>'

  scope :primary_region_code, ->(code) {
    joins(:primary_region).where("geobase_regions.code = '#{code}'").readonly(false)
  }

  def self.locality_and_primary_region_name(locality_name, region_name)
    joins(:primary_region).where("LOWER(geobase_localities.name) = ? AND LOWER(geobase_regions.name) = ?", locality_name.downcase, region_name.downcase).readonly(false).first
  end

  def tier
    return nil unless population

    if population > 500000
      1
    elsif population.between?(100000, 500000)
      2
    elsif population.between?(50000, 99999)
      3
    elsif population.between?(25000, 49999)
      4
    elsif population.between?(10000, 24999)
      5
    elsif population.between?(5000, 9999)
      6
    elsif population.between?(2500, 4999)
      7
    else
      8
    end
  end

  def nickname_array
    nicknames.to_s.split(SEPARATOR)
  end

  def random_nickname
    nickname_array[rand(nickname_array.length)]
  end

  def random_zip_code
    self.zip_codes.shuffle.first.try(:code)
  end

	def self.by_id(id)
    return all unless id.present?
    where("geobase_localities.id = ?", id)
  end

  def self.by_primary_region_id(primary_region_id)
    return all unless primary_region_id.present?
    where("geobase_localities.primary_region_id = ?", primary_region_id)
  end

  def self.by_name(name)
    return all unless name.present?
    where('LOWER(geobase_localities.name) LIKE ?', "#{name.downcase}%")
  end
end
